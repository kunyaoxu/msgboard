// --- for csrftoken ---
// ref: https://docs.djangoproject.com/en/1.11/ref/csrf/
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
// --- for csrftoken end---

let first_msg_id = -1, last_msg_id = -1;

const sendMsg = () => {
    //console.log("#send_btn or enter was pressed!");
    let msg_str = $("#msg_input").val().trim();
    $("#msg_input").val("");
    if (msg_str != "") {
        $.ajax({
            url: "/",
            method: "POST",
            data: { "msg": msg_str, "last_msg_id": last_msg_id }
        }).done((data)=>{
            //console.log(data);
            $("#msg_list").append(data);
            last_msg_id = $("#msg_list .msg:last-child").data("postid");
            let $msg_all = $("#msg_all");
            $msg_all.prop("scrollTop", $msg_all .prop("scrollHeight"));
        });
    }
}

const get_preMsg = (e) => {
    e.target.disabled = true;
    $.ajax({
        url: "/",
        method: "POST",
        data: { "first_msg_id": first_msg_id }
    }).done((data)=>{
        //console.log(data);
        $("#msg_list").prepend(data);
        let old_first_msg_id = first_msg_id;
        first_msg_id = parseInt($("#msg_list>.msg:first-child").data("postid"));
        if (old_first_msg_id - first_msg_id < 15) {
            //跑進來這邊表示已經讀完留言版的內容了，所以就讓按鈕消失
            $("#" + e.target.id).hide();
            return;
        }
        setTimeout(() => { e.target.disabled = false; }, 300);
    });
}

//console.log("hello i am my.js");
// --- send msg
$("#send_btn").on("click", sendMsg);
$("#msg_input").on("keydown", (e) =>{
	if( e.key == "Enter"){
		//console.log(e.key);
		sendMsg();
	}
});
// ------------

//--- more previous msg---
$("#more_btn").on("click", get_preMsg);
// ------------

// on page loaded
$(window).on("load", () => {
    first_msg_id = parseInt($("#msg_list>.msg:first-child").data("postid")) || -1;
    last_msg_id = parseInt($("#msg_list>.msg:last-child").data("postid")) || -1;
    if (last_msg_id - first_msg_id >= 14) {
        $("#more_btn").css("display","block");
    }
    //console.log("first_msg_id :" + first_msg_id + ", last_msg_id : " + last_msg_id);
});
