# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse, JsonResponse
from django.views import View
from django.shortcuts import render
from .models import Post

class main(View):
	def __init__(self):
		self.all_post = Post.objects.all()
		print("__init__")

	def get(self, request, *args, **kwargs):
		print(request.method)
		if (request.method == "GET"):
			post_len = len(self.all_post)
			if post_len < 15:
				send_post = self.all_post
			else :
				send_post = self.all_post[post_len - 15:post_len]
			return render(request, "index.html", { "Posts": send_post })

	def post(self, request, *args, **kwargs):
		print(request.method)
		msg = request.POST.get("msg")
		if msg != None:
			last_msg_id = int(request.POST.get("last_msg_id"))
			pp = Post(text = msg)
			new_pk_num = pp.publish()
			return_post = self.all_post.filter(pk__gt = last_msg_id).filter(pk__lte = new_pk_num)
			return render (request, "return_msg.html", { "Posts": return_post })
		else :
			first_msg_id = int(request.POST.get("first_msg_id"))
			return_post = self.all_post.filter(pk__gte = first_msg_id - 15).filter(pk__lt = first_msg_id)
			return render (request, "return_msg.html", { "Posts": return_post })
