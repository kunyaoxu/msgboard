def cal_factorial(num):
    if not isinstance(num, int):
        raise TypeError('Invalid value, please input a integer')
    if num == 0:
        return 1
    def cal(n):
            return n * cal(n - 1) if n != 1 else 1
    pass
    return cal(num)

#print cal_factorial(6)

